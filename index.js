const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4004;

app.use(express.json());

app.use(express.urlencoded({extended:true}));


//connecting mongoDB

mongoose.connect("mongodb+srv://joram_182:joramape182@zuitt-bootcamp.kq3szvv.mongodb.net/s35-activity?retryWrites=true&w=majority", {

		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cload database"));


const taskSchema = new mongoose.Schema({
	username : String,
	password : String,
	status : {
		type1 : String,
		type2 : String
	}
})

const Task = mongoose.model("Task", taskSchema);

app.post("/signup", (req, res) => {

	Task.findOne({username: req.body.username}, {password: req.body.password}, (error, result) => {
		if(result != null && result.username == req.body.username && result.password == req.body.password){
			return res.send('Duplicate signup found')
		}

		let newTask = new Task({
			username: req.body.username,
			password: req.body.password
		})

		newTask.save((error, savedTask) => {

			if(error){
				return console.log(error)
			}else {
				return res.status(200).send('New Task Created')
			}

		})
	})
})


app.listen(port, () => (console.log(`Server is now running at port ${port}`)));